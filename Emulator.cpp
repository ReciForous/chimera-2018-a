///////////////////////////////////////////////////////////////////////////
/// @author: Mohamed Zain Riyaz                                        ////
/// @date: 16-12-2017                                                  ////
/// @lastRevisiondate: 03-12-2017                                      ////
///////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#if defined(WIN32)
#include <winsock2.h>
#else 
// *NIX BASED OS INCLUDES
#include <errno.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
// FIXES FOR *NIX BASED OS INSTEAD OF BEING WINDOWS CENTRIC
typedef int SOCKET;
typedef sockaddr_in SOCKADDR_IN;
typedef sockaddr SOCKADDR;
typedef char _TCHAR;
#define SOCKET_ERROR (-1)
#define closesocket close
int fopen_s(FILE **f, const char *name, const char *mode){
    int ret = 0;
    assert(f);
    *f = fopen(name, mode);
    /* Can't be sure about 1-to-1 mapping of errno and MS' errno_t */
    if (!*f)
        ret = errno;
    return ret;
}
#endif


#pragma comment(lib, "wsock32.lib")


#define STUDENT_NUMBER    "mz2-riyaz"

#define IP_ADDRESS_SERVER "127.0.0.1"

#define PORT_SERVER 0x1984 // We define a port that we are going to use.
#define PORT_CLIENT 0x1985 // We define a port that we are going to use.

#define WORD  unsigned short
#define DWORD unsigned long
#define BYTE  unsigned char

#define MAX_FILENAME_SIZE 500
#define MAX_BUFFER_SIZE   500

SOCKADDR_IN server_addr;
SOCKADDR_IN client_addr;

SOCKET sock;  // This is our socket, it is the handle to the IO address to read/write packets

#if defined(WIN32)
WSADATA data;
#endif

char InputBuffer [MAX_BUFFER_SIZE];

char hex_file [MAX_BUFFER_SIZE];
char trc_file [MAX_BUFFER_SIZE];

//////////////////////////
//   Registers          //
//////////////////////////

#define FLAG_Z  0x80
#define FLAG_V  0x40
#define FLAG_I  0x08
#define FLAG_N  0x02
#define FLAG_C  0x01
#define REGISTER_A	5
#define REGISTER_F	4
#define REGISTER_E	3
#define REGISTER_D	2
#define REGISTER_C	1
#define REGISTER_B	0
WORD BaseRegister;
BYTE IndexRegister;

BYTE Registers[6];
BYTE Flags;
WORD ProgramCounter;
WORD StackPointer;


////////////
// Memory //
////////////

#define MEMORY_SIZE	65536

BYTE Memory[MEMORY_SIZE];

#define TEST_ADDRESS_1  0x01FA
#define TEST_ADDRESS_2  0x01FB
#define TEST_ADDRESS_3  0x01FC
#define TEST_ADDRESS_4  0x01FD
#define TEST_ADDRESS_5  0x01FE
#define TEST_ADDRESS_6  0x01FF
#define TEST_ADDRESS_7  0x0200
#define TEST_ADDRESS_8  0x0201
#define TEST_ADDRESS_9  0x0202
#define TEST_ADDRESS_10  0x0203
#define TEST_ADDRESS_11  0x0204
#define TEST_ADDRESS_12  0x0205


///////////////////////
// Control variables //
///////////////////////

bool memory_in_range = true;
bool halt = false;


///////////////////////
// Disassembly table //
///////////////////////

char opcode_mneumonics[][14] =
{
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"JP abs       ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"LDX  #       ", 
"LDX abs      ", 
"LDX abs,X    ", 
"LDX zpg      ", 
"LDX (ind)    ", 
"LDX bas      ", 
"RTN impl     ", 

"ADD A,B      ", 
"ADD A,C      ", 
"ADD A,D      ", 
"ADD A,E      ", 
"ADD A,F      ", 
"JCC abs      ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"LODS  #      ", 
"LODS abs     ", 
"LODS abs,X   ", 
"LODS zpg     ", 
"LODS (ind)   ", 
"LODS bas     ", 
"RCC impl     ", 

"SBB A,B      ", 
"SBB A,C      ", 
"SBB A,D      ", 
"SBB A,E      ", 
"SBB A,F      ", 
"JCS abs      ", 
"TST abs      ", 
"TST abs,X    ", 
"TSTA A,A     ", 
"LDZ  #       ", 
"LDZ abs      ", 
"LDZ abs,X    ", 
"LDZ zpg      ", 
"LDZ (ind)    ", 
"LDZ bas      ", 
"RCS impl     ", 

"CMP A,B      ", 
"CMP A,C      ", 
"CMP A,D      ", 
"CMP A,E      ", 
"CMP A,F      ", 
"JNE abs      ", 
"INC abs      ", 
"INC abs,X    ", 
"INCA A,A     ", 
"ILLEGAL     ", 
"STA abs      ", 
"STA abs,X    ", 
"STA zpg      ", 
"STA (ind)    ", 
"STA bas      ", 
"RNE impl     ", 

"OR A,B       ", 
"OR A,C       ", 
"OR A,D       ", 
"OR A,E       ", 
"OR A,F       ", 
"JEQ abs      ", 
"DEC abs      ", 
"DEC abs,X    ", 
"DECA A,A     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"REQ impl     ", 

"AND A,B      ", 
"AND A,C      ", 
"AND A,D      ", 
"AND A,E      ", 
"AND A,F      ", 
"JVC abs      ", 
"RR abs       ", 
"RR abs,X     ", 
"RRA A,A      ", 
"MV A,A       ", 
"MV A,B       ", 
"MV A,C       ", 
"MV A,D       ", 
"MV A,E       ", 
"MV A,F       ", 
"RVC impl     ", 

"EOR A,B      ", 
"EOR A,C      ", 
"EOR A,D      ", 
"EOR A,E      ", 
"EOR A,F      ", 
"JVS abs      ", 
"RLC abs      ", 
"RLC abs,X    ", 
"RLCA A,A     ", 
"MV B,A       ", 
"MV B,B       ", 
"MV B,C       ", 
"MV B,D       ", 
"MV B,E       ", 
"MV B,F       ", 
"RVS impl     ", 

"STX abs      ", 
"STX abs,X    ", 
"STX zpg      ", 
"STX (ind)    ", 
"STX bas      ", 
"JMI abs      ", 
"ASL abs      ", 
"ASL abs,X    ", 
"ASLA A,A     ", 
"MV C,A       ", 
"MV C,B       ", 
"MV C,C       ", 
"MV C,D       ", 
"MV C,E       ", 
"MV C,F       ", 
"RMI impl     ", 

"ILLEGAL     ", 
"ADCP A,C     ", 
"SBCP A,C     ", 
"ADI  #       ", 
"ILLEGAL     ", 
"JPL abs      ", 
"ASR abs      ", 
"ASR abs,X    ", 
"ASRA A,A     ", 
"MV D,A       ", 
"MV D,B       ", 
"MV D,C       ", 
"MV D,D       ", 
"MV D,E       ", 
"MV D,F       ", 
"RPL impl     ", 

"DEX impl     ", 
"XCHG A,C     ", 
"ILLEGAL     ", 
"SBI  #       ", 
"LD  #,B      ", 
"JLS abs      ", 
"LSR abs      ", 
"LSR abs,X    ", 
"LSRA A,A     ", 
"MV E,A       ", 
"MV E,B       ", 
"MV E,C       ", 
"MV E,D       ", 
"MV E,E       ", 
"MV E,F       ", 
"RHI impl     ", 

"INX impl     ", 
"CLC impl     ", 
"ILLEGAL     ", 
"CPI  #       ", 
"LD  #,C      ", 
"JLT abs      ", 
"NOT abs      ", 
"NOT abs,X    ", 
"NOTA A,A     ", 
"MV F,A       ", 
"MV F,B       ", 
"MV F,C       ", 
"MV F,D       ", 
"MV F,E       ", 
"MV F,F       ", 
"RLE impl     ", 

"DEZ impl     ", 
"SEC impl     ", 
"ILLEGAL     ", 
"ORI  #       ", 
"LD  #,D      ", 
"MAX impl     ", 
"NEG abs      ", 
"NEG abs,X    ", 
"NEGA A,0     ", 
"STS abs      ", 
"STS abs,X    ", 
"STS zpg      ", 
"STS (ind)    ", 
"STS bas      ", 
"ILLEGAL     ", 
"ILLEGAL     ", 

"INZ impl     ", 
"CLI impl     ", 
"ILLEGAL     ", 
"ANI  #       ", 
"LD  #,E      ", 
"MXA impl     ", 
"ROL abs      ", 
"ROL abs,X    ", 
"ROLA A,A     ", 
"STZ abs      ", 
"STZ abs,X    ", 
"STZ zpg      ", 
"STZ (ind)    ", 
"STZ bas      ", 
"ILLEGAL     ", 
"ILLEGAL     ", 

"ILLEGAL     ", 
"STI impl     ", 
"ILLEGAL     ", 
"XRI  #       ", 
"LD  #,F      ", 
"CSA impl     ", 
"RAR abs      ", 
"RAR abs,X    ", 
"RARA A,A     ", 
"LDA  #       ", 
"LDA abs      ", 
"LDA abs,X    ", 
"LDA zpg      ", 
"LDA (ind)    ", 
"LDA bas      ", 
"ILLEGAL     ", 

"ILLEGAL     ", 
"SEV impl     ", 
"NOP impl     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"PSH  ,A      ", 
"PSH  ,s      ", 
"PSH  ,B      ", 
"PSH  ,C      ", 
"PSH  ,D      ", 
"PSH  ,E      ", 
"PSH  ,F      ", 
"SWI impl     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 

"ILLEGAL     ", 
"CLV impl     ", 
"HLT impl     ", 
"CALL abs     ", 
"ILLEGAL     ", 
"POP A,       ", 
"POP s,       ", 
"POP B,       ", 
"POP C,       ", 
"POP D,       ", 
"POP E,       ", 
"POP F,       ", 
"RTI impl     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 
"ILLEGAL     ", 

}; 

////////////////////////////////////////////////////////////////////////////////
///                           Simulator/Emulator (Start)                     ///
////////////////////////////////////////////////////////////////////////////////
BYTE fetch()
{
	BYTE byte = 0;

	if ((ProgramCounter >= 0) && (ProgramCounter <= MEMORY_SIZE))
	{
		memory_in_range = true;
		byte = Memory[ProgramCounter];
		ProgramCounter++;
	}
	else
	{
		memory_in_range = false;
	}
	return byte;
}

/* function name - clear_flag_i
 * function type - void
 * description - force clears carry flag*/
void clear_flag_c(){
    Flags = Flags & (0xFF - FLAG_C);
}

/* function name - clear_flag_i
 * function type - void
 * description - force clears zero flag*/
void clear_flag_z(){
    Flags = Flags & (0xFF - FLAG_Z);
}

/* function name - clear_flag_i
 * function type - void
 * description - force clears negative flag*/
void clear_flag_n(){
    Flags = Flags & (0xFF - FLAG_N);
}

/* function name - clear_flag_i
 * function type - void
 * description - force overflow interrupt flag*/
void clear_flag_v(){
    Flags = Flags & (0xFF - FLAG_V);
}

/* function name - clear_flag_i
 * function type - void
 * description - force clears interrupt flag*/
void clear_flag_i(){
    Flags = Flags & (0xFF - FLAG_I);
}

/* function name - set_flag_z
 * function type - void
 * param number - 1
 * params - Word inReg
 * description - sets the zero flag based on the Register used for the argument*/
void set_flag_z(BYTE inReg) {
	BYTE reg; 
	reg = inReg; 

	if (reg == 0x00) // msbit set
	{
        //Set zero
        Flags = Flags | FLAG_Z;
	}
	else 
	{
        //Clear zero
        clear_flag_z();
	}
}

/* function name - set_flag_n
 * function type - void
 * param number - 1
 * params - void inReg
 * description - sets the negative flag based on the Register used for the argument*/
void set_flag_n(BYTE inReg){
    BYTE reg;
    reg = inReg;

    if ((reg & 0x80) != 0){ // msbit set
        //Set negative
        Flags = Flags | FLAG_N;
    }
    else{
        //Clear negative
        clear_flag_n();
    }
}

/* function name - set_flag_v
 * function type - void
 * param number - 4
 * params - BYTE in1, BYTE in2, BYTE out1, int instructions
 * description - sets the overflow flag based on the Registers used for the argument along with with an output register
 * final param acts a boolean to check whether function is being used in add or sbb/cmp instructions*/
void set_flag_v(BYTE in1, BYTE in2, BYTE out1, int instructions){
    BYTE reg1in;
    BYTE reg2in;
    BYTE regout;
    reg1in = in1;
    reg2in = in2;
    regout = out1;

    if(instructions != 1){

        reg2in = (~reg2in) + 1;
    }

    if((((reg1in & 0x80) == 0x80) && ((reg2in & 0x80) == 0x80) && ((regout & 0x80) != 0x80)) ||
       ((reg1in & 0x80) != 0x80) && ((reg2in & 0x80) != 0x80) && ((regout & 0x80) == 0x80) ){
        //Sets overflow
        Flags = Flags | FLAG_V;
    }
    else{
        //Clear overflow
        clear_flag_v();
    }
}

/* function name - set_flag_c
 * function type - void
 * param number - 1
 * params - WORD inReg
 * description - sets the carry flag based on the Register used for the argument*/
void set_flag_c(WORD inReg){
    //Might not work
    WORD reg;
    reg = inReg;

    if(reg >= 0x100){
        //Set carry
        Flags = Flags | FLAG_C;
    }
    else{
        //Clear carry
        clear_flag_c();
    }
}

/* function name - abs_x_addressing
 * function type - static
 * description - Creates the address for absolute with index register addressing modes and returns the address on call*/
static WORD abs_x_addressing(){
    BYTE LB;
    BYTE HB;
    WORD address = 0;
    address += IndexRegister;
    LB = fetch();
    HB = fetch();
    address += (WORD)((WORD)HB << 8) + LB;

    return address;
}

/* function name - abs_addressing
 * function type - static
 * description - Creates the address for absolute addressing modes and returns the address on call*/
static WORD abs_addressing(){
    BYTE HB;
    BYTE LB;
    WORD address = 0;

    LB = fetch();
    HB = fetch();
    address += (WORD)((WORD)HB << 8) + LB;

    return address;
}

/* function name - ind_addressing
 * function type - static
 * description - Creates the address for indirect addressing modes and returns the address on call*/
static WORD ind_addressing(){
    BYTE HB;
    BYTE LB;
    WORD address = 0;

    LB = fetch();
    HB = fetch();
    address = (WORD)((WORD)HB << 8) + LB;
    LB = Memory[address];
    HB = Memory[address + 1];
    address = (WORD)((WORD)HB << 8) + LB;

    return address;
}

/* function name - bas_addressing
 * function type - static
 * description - Creates the address for base addressing modes and returns the address on call*/
static WORD bas_addressing(){
    BYTE LB;
    WORD address = 0;

    if((LB = fetch()) >= 0x80) {
        LB = 0x00 - LB;
        address += (BaseRegister - LB);
    }
    else{
        address += (BaseRegister + LB) ;
    }

    return address;
}

/* function name - add_settings
 * function type - void
 * params - BYTE param1, BYTE param2
 * description - settings for addition instructions which finds the summation between two registers and sets the flags
 * based on the summation, the summation is written back to the accumulator*/
void add_settings(BYTE param1, BYTE param2){
    WORD temp_word;
    temp_word = (WORD)param1 + (WORD)param2;

    if((Flags & FLAG_C) == FLAG_C){
        temp_word++;
    }

    Registers[REGISTER_A] = (BYTE)temp_word;

    //Set or clear flags based on add result
    set_flag_c(temp_word);
    set_flag_z((BYTE)temp_word);
    set_flag_n((BYTE)temp_word);
    set_flag_v(param1, param2, (BYTE)temp_word, 1);

}

/* function name - sbb_settings
 * function type - void
 * params - BYTE param1, BYTE param2
 * description - settings for subtraction instructions which finds the difference between two registers and sets the flags
 * based on the diference, the difference is written back to the accumulator*/
void sbb_settings(BYTE param1, BYTE param2){
    WORD temp_word;
    temp_word = (WORD)param1 - (WORD)param2;

    if((Flags & FLAG_C) == FLAG_C){
        temp_word--;
    }

    Registers[REGISTER_A] = (BYTE)temp_word;

    //Set or clear flag based on sbb result
    set_flag_c(temp_word);
    set_flag_z((BYTE)temp_word);
    set_flag_n((BYTE)temp_word);
    set_flag_v(param1, param2, (BYTE)temp_word, 0);
}

/* function name - cmp_settings
 * function type - void
 * params - BYTE param1, BYTE param2
 * description - settings for compare instructions which finds the difference between two registers and sets the flags
 * based on the diference, the results are never written back to any location */
void cmp_settings(BYTE param1, BYTE param2){
    WORD temp_word;
    temp_word = (WORD)param1 - (WORD)param2;

    //set flags on compare result
    set_flag_c(temp_word);
    set_flag_z((BYTE)temp_word);
    set_flag_n((BYTE)temp_word);
    set_flag_v(param1, param2, (BYTE)temp_word, 0);
}

/* function name - and_settings
 * function type - void
 * params - BYTE param1, BYTE param2
 * description - settings for bit wise and instructions, carries out bitwise and operation between two registers and
 * writes the result to the accumulator*/
void and_settings(BYTE param1, BYTE param2){
    WORD temp_word;
    temp_word = (WORD)param1 & (WORD)param2;

    //set flags
    set_flag_z((BYTE)temp_word);
    set_flag_n((BYTE)temp_word);

    Registers[REGISTER_A] = (BYTE)temp_word;
}

/* function name - or_settings
 * function type - void
 * params - BYTE param1, BYTE param2
 * description - settings for bit wise inclusive or instructions, carries out bitwise inclustive or operation between
 * two registers and writes result to the accumulator*/
void or_settings(BYTE param1, BYTE param2){
    WORD temp_word;
    temp_word = (WORD)param1 | (WORD)param2;

    //set flags
    set_flag_z((BYTE)temp_word);
    set_flag_n((BYTE)temp_word);

    Registers[REGISTER_A] = (BYTE)temp_word;
}

/* function name - or_settings
 * function type - void
 * params - BYTE param1, BYTE param2
 * description - settings for bit wise exclusive or instructions, carries out bitwise exclusive or operation between two
 * registers and writes result to the accumulator*/
void eor_settings(BYTE param1, BYTE param2){
    WORD temp_word;
    temp_word = (WORD)param1 ^ (WORD)param2;

    //set flags
    set_flag_z((BYTE)temp_word);
    set_flag_n((BYTE)temp_word);

    Registers[REGISTER_A] = (BYTE)temp_word;
}

/* function name - jp_settings
 * function type - void
 * description - settings for jump instructions*/
void jp_settings(){
    WORD address = 0;
    address = abs_addressing();
    ProgramCounter = address;
}

/* function name - jp_settings
 * function type - void
 * description - settings for return instructions*/
void rtn_settings(){
    BYTE HB = 0;
    BYTE LB = 0;
    if((StackPointer >= 0) && (StackPointer < MEMORY_SIZE - 2)){
        LB = Memory[StackPointer];
        StackPointer++;
        HB = Memory[StackPointer];
        StackPointer++;
    }
    ProgramCounter = ((WORD)HB << 8) + (WORD)LB;
}

/* function name - jp_settings
 * function type - void
 * params - BYTE in_reg
 * description - settings for push instructions*/
void psh_settings(BYTE in_reg){
    if ((StackPointer >= 1) && (StackPointer < MEMORY_SIZE)){
        StackPointer --;
        Memory[StackPointer] = in_reg;
    }
}

/* function name - jp_settings
 * function type - void
 * params - BYTE, in_reg
 * description - settings for pop instructions*/
void pop_settings(BYTE in_reg){
    if ((StackPointer >= 0) && (StackPointer < MEMORY_SIZE - 1)){
        in_reg = Memory[StackPointer];
        StackPointer ++;
    }
}

void Group_1(BYTE opcode){
	BYTE LB = 0;
	BYTE HB = 0;
	WORD address = 0;
	WORD data = 0;
    WORD temp_word = 0;
    WORD param1_16 = 0;
    WORD param2_16 = 0;
    BYTE param1 = 0;
    BYTE param2 = 0;
    BYTE saved_flags = 0;
    int z = 0;
    int n = 0;
    int c = 0;
    int v = 0;
    int i = 0;

	switch(opcode) {
		case 0xD9: /// <----- LDA #
			data = fetch();
            Registers[REGISTER_A] = data;
            //set flags
            clear_flag_c();
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
			break;

        case 0xDA: // LDA - abs
            address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Registers[REGISTER_A] = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(Registers[REGISTER_A]);
                set_flag_n(Registers[REGISTER_A]);
            }
            break;

        case 0xDB: // LDA - abs,X
            address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Registers[REGISTER_A] = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(Registers[REGISTER_A]);
                set_flag_n(Registers[REGISTER_A]);
            }
            break;

        case 0xDC: // LDA - zpg
            address += 0x0000 | (WORD)fetch();
            if(address >= 0 && address < MEMORY_SIZE) {
                Registers[REGISTER_A] = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(Registers[REGISTER_A]);
                set_flag_n(Registers[REGISTER_A]);
            }
            break;

        case 0xDD: // LDA - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Registers[REGISTER_A] = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(Registers[REGISTER_A]);
                set_flag_n(Registers[REGISTER_A]);
            }
            break;

        case 0xDE: // LDA - bas
            address = bas_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                Registers[REGISTER_A] = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(Registers[REGISTER_A]);
                set_flag_n(Registers[REGISTER_A]);
            }
            break;

        case 0x09: /// <----- LDX - #
            data = fetch();
            IndexRegister = data;
            //set flags
            clear_flag_c();
            set_flag_z(IndexRegister);
            set_flag_n(IndexRegister);
            break;

        case 0x0A: // LDX - abs
           address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                IndexRegister = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(IndexRegister);
                set_flag_n(IndexRegister);

            }
            break;

        case 0x0B: // LDX - abs,X
            address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                IndexRegister = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(IndexRegister);
                set_flag_n(IndexRegister);

            }
            break;

        case 0x0C: // LDX - zpg
            address += 0x0000 | (WORD)fetch();
            if(address >= 0 && address < MEMORY_SIZE) {
                IndexRegister = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(IndexRegister);
                set_flag_n(IndexRegister);

            }
            break;

        case 0x0D: // LDX - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                IndexRegister = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(IndexRegister);
                set_flag_n(IndexRegister);

            }
            break;

        case 0x0E: // LDX - bas
            address = bas_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                IndexRegister = Memory[address];
                //set flags
                clear_flag_c();
                set_flag_z(IndexRegister);
                set_flag_n(IndexRegister);

            }
            break;

        case 0x29: /// <----- LDZ - #
            LB = fetch();
            HB = fetch();

            data = (WORD)((WORD)HB << 8) + LB;
            BaseRegister = data;
            break;

        case 0x2A: // LDZ - abs
           address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                BaseRegister = Memory[address];
            }
            break;

        case 0x2B: // LDZ - abs,X
           address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                BaseRegister = Memory[address];
            }
            break;

        case 0x2C: // LDZ - zpg
            address += 0x0000 | (WORD)fetch();
            if(address >= 0 && address < MEMORY_SIZE) {
                BaseRegister = Memory[address];
            }
            break;

        case 0x2D: // LDZ - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                BaseRegister = Memory[address];
            }
            break;

        case 0x2E: // LDZ - bas
            address = bas_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                BaseRegister = Memory[address];
            }
            break;

        case 0x19: /// <----- LODS - #
            data = fetch();
            StackPointer = data ;
            StackPointer += (WORD)fetch() << 8;
            //set flags
            clear_flag_c();
            set_flag_z((BYTE)StackPointer);
            set_flag_n((BYTE)StackPointer); /// Making fixes in the flags for LODS typecasting to byte to check

            break;

        case 0x1A: // LODS - abs
           address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                StackPointer = Memory[address];
                StackPointer += (WORD)Memory[address + 1] << 8;
                //set flags
                clear_flag_c();
                set_flag_z((BYTE)StackPointer);
                set_flag_n((BYTE)StackPointer);
            }
            break;

        case 0x1B: // LODS - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                StackPointer = Memory[address];
                StackPointer += (WORD)Memory[address + 1] << 8;
                //set flags
                clear_flag_c();
                set_flag_z((BYTE)StackPointer);
                set_flag_n((BYTE)StackPointer);
            }
            break;

        case 0x1C: // LODS - zpg
            address += 0x0000 | (WORD)fetch();

            if(address >= 0 && address < MEMORY_SIZE) {
                StackPointer = Memory[address];
                StackPointer += ((WORD)Memory[address + 1] << 8);
                //set flags
                clear_flag_c();
                set_flag_z((BYTE)StackPointer);
                set_flag_n((BYTE)StackPointer);

            }
            break;

        case 0x1D: // LODS - ind
            address = ind_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1){
                StackPointer = Memory[address];
                StackPointer += (WORD)Memory[address + 1] << 8;
            }
            //set flags
            clear_flag_c();
            set_flag_z((BYTE)StackPointer);
            set_flag_n((BYTE)StackPointer);
            break;

        case 0x1E: // LODS - bas
            address = bas_addressing();
            if(address >= 0 && address < MEMORY_SIZE - 1) {
                StackPointer = Memory[address];
                StackPointer += (WORD)Memory[address + 1] << 8;
            }
            // set flags
            clear_flag_c();
            set_flag_z((BYTE)StackPointer);
            set_flag_n((BYTE)StackPointer);
            break;

        case 0x3A: /// <----- STA - abs
           address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = Registers[REGISTER_A];
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x3B: // STA - abs,X
           address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = Registers[REGISTER_A];
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0x3C: // STA - zpg
            address += 0x0000 | (WORD)fetch();

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = Registers[REGISTER_A];
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0x3D: // STA - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = Registers[REGISTER_A];
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x3E: // STA - bas
           address = bas_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = Registers[REGISTER_A];
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x70: /// <----- STX - abs
           address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = IndexRegister;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0x71: // STX - abs,X
            address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = IndexRegister;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0x72: // STX - zpg
            address += 0x0000 | (WORD)fetch();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = IndexRegister;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x73: // STX - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = IndexRegister;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x74: // STX - bas
            address = bas_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = IndexRegister;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0xC9: /// <----- STZ - abs
           address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = BaseRegister;
            }
            break;

        case 0xCA: //  STZ - abs,X
           address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = BaseRegister;
            }
            break;

        case 0xCB: // STZ - zpg
            address += 0x0000 | (WORD)fetch();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = BaseRegister;
            }
            break;

        case 0xCC: // STZ - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = BaseRegister;
            }
            break;

        case 0xCD: // STZ - bas
            if((LB = fetch()) >= 0x80) {
                LB = 0x00 - LB;
                address += (BaseRegister - LB);
            }
            else{
                address += (BaseRegister + LB) ;
            }

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = BaseRegister;
            }
            break;

        case 0xB9: /// <----- STS - abs
           address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = (BYTE)StackPointer;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0xBA: // STS - abs,X
            address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE - 1) {
                Memory[address] = (BYTE)StackPointer;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0xBB: // STS - zpg
            address += 0x0000 | (WORD)fetch();
            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = (BYTE)StackPointer;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0xBC: // STS - ind
            address = ind_addressing();
            if(address >= 0 && address < MEMORY_SIZE){
                Memory[address] = (BYTE)StackPointer;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0xBD: // STS - bas
            address = bas_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                Memory[address] = (BYTE)StackPointer;
            }
            //set flags
            clear_flag_c();
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x94: /// <----- LD - # reg B
            data = fetch();
            Registers[REGISTER_B] = (BYTE)data;

            //set flags
            clear_flag_c();
            set_flag_z(Registers[REGISTER_B]);
            set_flag_n(Registers[REGISTER_B]);
            break;

        case 0xA4: // LD - # reg C
            data = fetch();
            Registers[REGISTER_C] = (BYTE)data;

            //set flags
            clear_flag_c();
            set_flag_z(Registers[REGISTER_C]);
            set_flag_n(Registers[REGISTER_C]);
            break;

        case 0xB4: // LD - # Reg D
            data = fetch();
            Registers[REGISTER_D] = (BYTE)data;

            //set flags
            clear_flag_c();
            set_flag_z(Registers[REGISTER_D]);
            set_flag_n(Registers[REGISTER_D]);
            break;

        case 0xC4: // LD - # Reg E
            data = fetch();
            Registers[REGISTER_E] = (BYTE)data;

            //set flags
            clear_flag_c();
            set_flag_z(Registers[REGISTER_E]);
            set_flag_n(Registers[REGISTER_E]);
            break;

        case 0xD4: // LD - # Reg F
            data = fetch();
            Registers[REGISTER_F] = (BYTE)data;

            //set flags
            clear_flag_c();
            set_flag_z(Registers[REGISTER_F]);
            set_flag_n(Registers[REGISTER_F]);
            break;

        case 0xB5: /// <----- MAX - impl
            IndexRegister = Registers[REGISTER_A];
            set_flag_n(IndexRegister);
            break;

        case 0xC5: /// <----- MXA - impl
            Registers[REGISTER_A] = IndexRegister;
            break;

        case 0xD5: /// <----- CSA - impl
            Registers[REGISTER_A] = Flags;
            break;

        case 0x10: /// <----- ADD - add A-B
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_B];

            add_settings(param1, param2);
            break;

        case 0x11: // ADD - add A-C
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_C];

            add_settings(param1, param2);
            break;

        case 0x12: // ADD  - add A-D
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_D];

           add_settings(param1, param2);
            break;

        case 0x13: // ADD - add A-E
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_E];

            add_settings(param1, param2);
            break;

        case 0x14: // ADD - add A-F
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_F];

            add_settings(param1, param2);
            break;

        case 0x20: /// <----- SBB - subtract A-B
            param1 = (Registers[REGISTER_A]);
            param2 = Registers[REGISTER_B];

            sbb_settings(param1, param2);
            break;

        case 0x21: // SBB - subtract A-C
            param1 = (Registers[REGISTER_A]);
            param2 = Registers[REGISTER_C];

            sbb_settings(param1, param2);
            break;

        case 0x22: // SBB - subtract A-D
            param1 = (Registers[REGISTER_A]);
            param2 = Registers[REGISTER_D];

            sbb_settings(param1, param2);
            break;

        case 0x23: // SBB - subtract A-E
            param1 = (Registers[REGISTER_A]);
            param2 = Registers[REGISTER_E];

            sbb_settings(param1, param2);
            break;

        case 0x24: // SBB - subtract A-F
            param1 = (Registers[REGISTER_A]);
            param2 = Registers[REGISTER_F];

            sbb_settings(param1, param2);
            break;

        case 0x30: /// <----- CMP - A-B
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_B];

            cmp_settings(param1, param2);
            break;

        case 0x31: // CMP - A-C
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_C];

            cmp_settings(param1, param2);
            break;

        case 0x32: // CMP - A-D
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_D];

            cmp_settings(param1, param2);
            break;

        case 0x33: // CMP - A-E
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_E];

            cmp_settings(param1, param2);
            break;

        case 0x34: //  CMP - A-F
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_F];

            cmp_settings(param1, param2);
            break;

        case 0xA1: /// <----- CLC - impl
            clear_flag_c();
            break;

        case 0xB1: /// <----- SEC - impl
            Flags = Flags | FLAG_C;
            break;

        case 0xC1: /// <----- CLI - impl
            clear_flag_i();
            break;

        case 0xD1: /// <----- STI - impl
            Flags = Flags | FLAG_I;
            break;

        case 0xE1: /// <----- SEV - impl
            Flags = Flags | FLAG_V;
            break;

        case 0xF1: /// <----- CLV - impl
            clear_flag_v();
            break;

        case 0xE5: /// <----- PSH - reg A
            psh_settings(Registers[REGISTER_A]);
            break;

        case 0xE6: // PSH - Flags
            psh_settings(Flags);
            break;

        case 0xE7: // PSH - reg B
            psh_settings(Registers[REGISTER_B]);
            break;

        case 0xE8: // PSH - reg C
            psh_settings(Registers[REGISTER_C]);
            break;

        case 0xE9: // PSH - reg D
            psh_settings(Registers[REGISTER_D]);
            break;

        case 0xEA: // PSH - reg E
            psh_settings(Registers[REGISTER_E]);
            break;

        case 0xEB: // PSH - reg F
            psh_settings(Registers[REGISTER_F]);
            break;

        case 0xF5: /// <----- POP - reg A
            pop_settings(Registers[REGISTER_A]);
            break;

        case 0xF6: // POP - Flags
            pop_settings(Flags);
            break;

        case 0xF7: // POP - reg B
            pop_settings(Registers[REGISTER_B]);
            break;

        case 0xF8: // POP - reg C
            pop_settings(Registers[REGISTER_C]);
            break;

        case 0xF9: // POP - reg D
            pop_settings(Registers[REGISTER_D]);
            break;

        case 0xFA: // POP reg E
            pop_settings(Registers[REGISTER_E]);
            break;

        case 0xFB: // POP reg F
            pop_settings(Registers[REGISTER_F]);
            break;

        case 0xF3: /// <----- CALL - abs
            address = abs_addressing();

            if((StackPointer >= 2) && (StackPointer < MEMORY_SIZE)){
                StackPointer --;
                Memory[StackPointer] = (BYTE)((ProgramCounter >> 8) & 0xFF);
                StackPointer --;
                Memory[StackPointer] = (BYTE)(ProgramCounter & 0xFF);
            }

            ProgramCounter = address;
            break;

        case 0x05: /// <----- JP - abs
            jp_settings();
            break;

        case 0x15: /// <----- JCC - abs
            if((Flags & FLAG_C) == 0x00){
                jp_settings();
            }
            break;

        case 0x25: /// <----- JCS - abs
            if((Flags & FLAG_C) == FLAG_C){
               jp_settings();
            }
            break;

        case 0x35: /// <----- JNE - abs
            if((Flags & FLAG_Z) == 0x00){
               jp_settings();
            }
            break;

        case 0x45: /// <----- JEQ - abs
            if((Flags & FLAG_Z) == FLAG_Z){
               jp_settings();
            }
            break;

        case 0x55: /// <----- JVC - abs
            if((Flags & FLAG_V) == 0x00){
               jp_settings();
            }
            break;

        case 0x65: /// <----- JVS - abs
            if((Flags & FLAG_V) == FLAG_V){
               jp_settings();
            }
            break;

        case 0x75: /// <----- JMI - abs
            if((Flags & FLAG_N) == FLAG_N){
               jp_settings();
            }
            break;

        case 0x85: /// <----- JPL - abs
            if((Flags & FLAG_N) == 0x00){
               jp_settings();
            }
            break;

        case 0x95: /// <----- JLS - abs
            if((Flags & FLAG_Z) == FLAG_Z){
                z = 1;
            }
            else{
                z = 0;
            }
            if((Flags & FLAG_N) == FLAG_N){
                n = 1;
            }else{
                n = 0;
            }
            if((Flags & FLAG_V) == FLAG_V){
                v = 1;
            }else{
                v = 0;
            }

            if(((z | n) ^ v) == 0){
               jp_settings();
            }
            break;

        case 0xA5: /// <----- JLT - abs
            if((Flags & FLAG_V) == FLAG_V){
                v = 1;
            }else{
                v = 0;
            }

            if((Flags & FLAG_N) == FLAG_N){
                n = 1;
            }else{
                n = 0;
            }

            if((n ^ v) == 1){
               jp_settings();
            }
            break;

        case 0x0F: /// <----- RTN - impl
            rtn_settings();
            break;

        case 0x1F: /// <----- RCC - impl
            if((Flags & FLAG_C) == 0x00){
              rtn_settings();
            }
            break;

        case 0x2F: /// <----- RCS - impl
            if((Flags & FLAG_C) == FLAG_C){
              rtn_settings();
            }
            break;

        case 0x3F: /// <----- RNE - impl
            if((Flags & FLAG_Z) == 0x00){
              rtn_settings();
            }
            break;

        case 0x4F: /// <----- REQ - impl
            if((Flags & FLAG_Z) == FLAG_Z){
              rtn_settings();
            }
            break;

        case 0x5F: /// <----- RVC - impl
            if((Flags & FLAG_V) == 0x00){
              rtn_settings();
            }
            break;

        case 0x6F: /// <----- RVS - impl
            if((Flags & FLAG_V) == FLAG_V){
              rtn_settings();
            }
            break;

        case 0x7F: /// <----- RMI - impl
            if((Flags & FLAG_N) == FLAG_N){
              rtn_settings();
            }
            break;

        case 0x8F: /// <----- RPL - impl
            if((Flags & FLAG_N) == 0x00){
              rtn_settings();
            }
            break;

        case 0x9F: /// <----- RHI - impl
            if(((Flags & FLAG_C) | (Flags & FLAG_Z)) != 0x00){
              rtn_settings();
            }
            break;

        case 0xAF: /// <----- RLE - impl
            if(((Flags & FLAG_C) | (Flags & FLAG_Z)) == 0x00){
              rtn_settings();
            }
            break;

        case 0x36: /// <----- INC - abs
            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                temp_word = Memory[address] + 1;
                Memory[address] = temp_word;
                //set flags
                set_flag_z(Memory[address]);
                set_flag_n(Memory[address]);
            }
            break;

        case 0x37: // INC - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                temp_word = Memory[address] + 1;
                Memory[address] = temp_word;
            }
            //set flags
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0x46: /// <----- DEC - abs
            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                temp_word = Memory[address] - 1;
                Memory[address] = temp_word;
            }
            //set flags
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);

            break;

        case 0x47: // DEC - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                temp_word = Memory[address] - 1;
                Memory[address] = temp_word;
                //set flags
                set_flag_z(Memory[address]);
                set_flag_n(Memory[address]);
            }
            break;

        case 0x38: /// <----- INCA - A
            temp_word = Registers[REGISTER_A] + 1;
            Registers[REGISTER_A] = temp_word;
            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0x48: /// <----- DECA - A
            temp_word = Registers[REGISTER_A] - 1;
            Registers[REGISTER_A] = temp_word;
            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0xA0: /// <----- INX - impl
            temp_word = IndexRegister + 1;
            IndexRegister = temp_word;
            //set flags
            set_flag_z(IndexRegister);
            break;

        case 0x90: /// <----- DEX  - impl
            param1 = IndexRegister;
            temp_word = (WORD)param1 - 1;
            IndexRegister = temp_word;
            //set flags
            set_flag_z(IndexRegister);
            break;

        case 0xC0: /// <----- INZ - impl
            param1 = BaseRegister;
            temp_word = (WORD)param1 + 1;
            BaseRegister = temp_word;
            //set flags
            set_flag_z((BYTE)BaseRegister);
            break;

        case 0xB0: /// <----- DEZ - impl
            param1 = BaseRegister;
            temp_word = (WORD)param1 - 1;
            BaseRegister = temp_word;
            //set flags
            set_flag_z((BYTE)BaseRegister);
            break;

        case 0x50: /// <----- AND - A-B
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_B];

            and_settings(param1, param2);
            break;

        case 0x51: // AND - A-C
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_C];

            and_settings(param1, param2);
            break;

        case 0x52: // AND - A-D
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_D];

            and_settings(param1, param2);
            break;

        case 0x53: // AND - A-E
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_E];

            and_settings(param1, param2);
            break;

        case 0x54: // AND - A-F
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_F];

            and_settings(param1, param2);
            break;

        case 0x40: /// <----- OR - A-B
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_B];

            or_settings(param1, param2);
            break;

        case 0x41: // OR - A-C
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_C];

            or_settings(param1, param2);
            break;

        case 0x42: // OR - A-D
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_D];

            or_settings(param1, param2);
            break;

        case 0x43: // OR - A-E
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_E];

            or_settings(param1, param2);
            break;

        case 0x44: // OR - A-F
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_F];

            or_settings(param1, param2);
            break;

        case 0x60: /// <----- EOR - A-B
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_B];

            eor_settings(param1, param2);
            break;

        case 0x61: // EOR - A-C
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_C];

            eor_settings(param1, param2);
            break;

        case 0x62: // EOR - A-D
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_D];

            eor_settings(param1, param2);
            break;

        case 0x63: // EOR - A-E
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_E];

            eor_settings(param1, param2);
            break;

        case 0x64: // EOR - A-F
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_F];

            eor_settings(param1, param2);
            break;

        case 0xA6: ///<----- NOT - abs
            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE){
                temp_word = ~Memory[address];
                Memory[address] = temp_word;
            }
            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_n((BYTE)temp_word);
            set_flag_c(temp_word);
            break;

        case 0xA7: // NOT - abs,X
            address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE - 1) {
                temp_word = ~Memory[address];
                Memory[address] = temp_word;
            }
            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_n((BYTE)temp_word);
            set_flag_c(temp_word);
            break;


        case 0x8A: /// <----- NOTA - A
            Registers[REGISTER_A] = ~(Registers[REGISTER_A]);

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            set_flag_c((WORD)Registers[REGISTER_A]);
            break;

        case 0xB6: /// <----- NEG - abs
            address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE) {
                temp_word = 0 - Memory[address];
                Memory[address] = temp_word;
            }

            //set flags
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0xB7: // - NEG abs,X
            address = abs_x_addressing();
            if(address >= 0 && address < MEMORY_SIZE - 1) {
                temp_word = 0 - Memory[address];
                Memory[address] = temp_word;
            }
            //set flags
            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0xB8: /// <----- NEGA - A
            temp_word = 0 - Registers[REGISTER_A];
            Registers[REGISTER_A] = temp_word;

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0x56: /// <----- RR - abs
            saved_flags = Flags;

            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                if ((Memory[address] & 0x01) == 0x01) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] >> 1) & 0x7F;

                //Carry LSB value to MSB
                if ((saved_flags & FLAG_C) == FLAG_C) {
                    Memory[address] = Memory[address] | 0x80;
                }
            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x57: // RR - abs,X
            saved_flags = Flags;

            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                if ((Memory[address] & 0x01) == 0x01) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] >> 1) & 0x7F;

                //Carry LSB value to MSB
                if ((saved_flags & FLAG_C) == FLAG_C) {
                    Memory[address] = Memory[address] | 0x80;
                }
            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x58: /// <----- RRA - A
            saved_flags = Flags;

            if((Registers[REGISTER_A] & 0x01) == 0x01){
                Flags = Flags | FLAG_C;
            }
            else{
                Flags = Flags & (0xFF - FLAG_C);
            }

            Registers[REGISTER_A] = (Registers[REGISTER_A] >> 1) & 0x7F;

            //Carry LSB value to MSB
            if ((saved_flags & FLAG_C) == FLAG_C){
                Registers[REGISTER_A] = Registers[REGISTER_A] | 0x80;
            }

            //set flags
            set_flag_n(Registers[REGISTER_A]);
            set_flag_z(Registers[REGISTER_A]);
            break;


        case 0x66: /// <----- RLC - abs
            saved_flags = Flags;

            address = abs_addressing();

            if(address >= address && address < MEMORY_SIZE) {
                //Transfer MSB to Carry
                if ((Memory[address] & 0x80) == 0x80) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] << 1) & 0xFE;

                //Carry MSB to LSB
                if ((saved_flags & FLAG_C) == FLAG_C) {
                    Memory[address] = Memory[address] | 0x01;
                }
            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x67: // RLC - abs,X
            saved_flags = Flags;

            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1){
                //Transfer MSB to Carry
                if ((Memory[address] & 0x80) == 0x80) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] << 1) & 0xFE;

                //Carry MSB to LSB
                if ((saved_flags & FLAG_C) == FLAG_C) {
                    Memory[address] = Memory[address] | 0x01;
                }
            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x68: /// <----- RLCA - A
            saved_flags = Flags;

            //Transfer MSB to Carry
            if((Registers[REGISTER_A] & 0x80) == 0x80){
                Flags = Flags | FLAG_C;
            }
            else{
                Flags = Flags & (0xFF - FLAG_C);
            }

            Registers[REGISTER_A] = (Registers[REGISTER_A] << 1) & 0xFE;

            //Carry MSB to LSB
            if ((saved_flags & FLAG_C) == FLAG_C){
                Registers[REGISTER_A] = Registers[REGISTER_A] | 0x01;
            }

            //set flags
            set_flag_n(Registers[REGISTER_A]);
            set_flag_z(Registers[REGISTER_A]);
            break;

        case 0x76: /// <----- ASL - abs
            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                //Carry off from MSB
                if ((Memory[address] & 0x80) == 0x80) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] << 1) & 0xFE;

            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x77: // ASL - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                //Carry off from MSB
                if ((Memory[address] & 0x80) == 0x80) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] << 1) & 0xFE;

            }

            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x78: /// <----- ASLA - A
            if((Registers[REGISTER_A] & 0x80) == 0x80){
                Flags = Flags | FLAG_C;
            }
            else{
                Flags = Flags & (0xFF - FLAG_C);
            }

            Registers[REGISTER_A] = (Registers[REGISTER_A] << 1) & 0xFE;

            //set flags
            set_flag_n(Registers[REGISTER_A]);
            set_flag_z(Registers[REGISTER_A]);
            break;

        case 0x86: /// <----- ASR - abs
            address = abs_addressing();
            if(address >= 0 && address < MEMORY_SIZE - 1) {
                //Carry off from LSB
                if ((Memory[address] & 0x01) == 0x01) {
                    Flags = Flags | FLAG_C;
                }
                else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] >> 1) & 0x7F;

                //add sign extension to MSB
                if ((Flags & FLAG_N) == FLAG_N) {
                    Memory[address] = Memory[address] | 0x80;
                }
            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x87: // ASR - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                //Carry off from LSB
                if ((Memory[address] & 0x01) == 0x01) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] >> 1) & 0x7F;

                //add sign extension to MSB
                if ((Flags & FLAG_N) == FLAG_N) {
                    Memory[address] = Memory[address] | 0x80;
                }
            }
            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x88: /// <----- ASRA - A
            //Carry off from LSB
            if ((Registers[REGISTER_A] & 0x01) == 0x01) {
                Flags = Flags | FLAG_C;
            }
            else {
                Flags = Flags & (0xFF - FLAG_C);
            }

            Registers[REGISTER_A] = (Registers[REGISTER_A] >> 1) & 0x7F;

            //add sign extension to MSB
            if((Flags & FLAG_N) == FLAG_N) {
                Registers[REGISTER_A] = Registers[REGISTER_A] | 0x80;
            }

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0x96: /// <----- LSR - abs
            address = abs_addressing();


            if(address >= 0 && address < MEMORY_SIZE) {
                //Carry of from LSB
                if ((Memory[address] & 0x01) == 0x01) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] >> 1) & 0x7F;
            }

            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x97: // LSR - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE - 1) {
                //Carry of from LSB
                if ((Memory[address] & 0x01) == 0x01) {
                    Flags = Flags | FLAG_C;
                } else {
                    Flags = Flags & (0xFF - FLAG_C);
                }

                Memory[address] = (Memory[address] >> 1) & 0x7F;
            }

            //set flags
            set_flag_n(Memory[address]);
            set_flag_z(Memory[address]);
            break;

        case 0x98: /// <----- LSRA - A
            //Carry off from LSB
            if((Registers[REGISTER_A] & 0x01) == 0x01){
                Flags = Flags | FLAG_C;
            }
            else{
                Flags = Flags & (0xFF - FLAG_C);
            }

            Registers[REGISTER_A] = (Registers[REGISTER_A] >> 1) & 0x7F;

            //set flags
            set_flag_n(Registers[REGISTER_A]);
            set_flag_z(Registers[REGISTER_A]);
            break;

        case 0xC6: /// <----- ROL - abs
            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                param1 = Memory[address] << 1;

                if ((Memory[address] & 0x80) == 0x80){
                    param1 = param1 | 0x01;
                }
            }

            Memory[address] = param1;

            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0xC7: // ROL - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                param1 = Memory[address] << 1;

                if ((Memory[address] & 0x80) == 0x80){
                    param1 = param1 | 0x01;
                }
            }

            Memory[address] = param1;

            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0xC8: /// <----- ROLA - A
            if(address >= 0 && address < MEMORY_SIZE) {
                param1 = Registers[REGISTER_A] << 1;

                if ((Registers[REGISTER_A] & 0x80) == 0x80){
                    param1 = param1 | 0x01;
                }
            }

            Registers[REGISTER_A] = param1;

            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0xD6: /// <----- RAR - abs
            address = abs_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                param1 = Memory[address] >> 1;

                if ((Memory[address] & 0x01) == 0x01){
                    param1 = param1 | 0x80;
                }
            }

            Memory[address] = param1;

            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0xD7: // RAR - abs,X
            address = abs_x_addressing();

            if(address >= 0 && address < MEMORY_SIZE) {
                param1 = Memory[address] >> 1;

                if ((Memory[address] & 0x01) == 0x01){
                    param1 = param1 | 0x80;
                }
            }

            Memory[address] = param1;

            set_flag_z(Memory[address]);
            set_flag_n(Memory[address]);
            break;

        case 0xD8: /// <----- RARA - A
            if(address >= 0 && address < MEMORY_SIZE) {
                param1 = Registers[REGISTER_A] >> 1;

                if ((Registers[REGISTER_A] & 0x01) == 0x01){
                    param1 = param1 | 0x80;
                }
            }

            Registers[REGISTER_A] = param1;

            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0x81: /// <----- ADCP - A-C
            param1_16 = Registers[REGISTER_C] + (WORD)(WORD) Registers[REGISTER_D] << 8;
            param2_16 = Registers[REGISTER_A];
            temp_word = param1_16 + param2_16;

            Registers[REGISTER_A] = temp_word;
            Registers[REGISTER_B] = (temp_word >> 8);
            break;

        case 0x82: /// <----- SBCP - A-C
            param1_16 = Registers[REGISTER_C] + (WORD)(WORD)Registers[REGISTER_D] << 8;
            param2_16 = Registers[REGISTER_A];
            temp_word = param1_16 - param2_16;

            Registers[REGISTER_A] = temp_word;
            Registers[REGISTER_B] = (temp_word >> 8);
            break;

        case 0x91: /// <----- XCHG - A-C
            param1 = Registers[REGISTER_A];
            param2 = Registers[REGISTER_C];


            Registers[REGISTER_A] = param2;
            Registers[REGISTER_C] = param1;

            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            clear_flag_c();
            break;

        case 0x83: /// <----- ADI - #
            param1 = Registers[REGISTER_A];
            data = fetch();
            temp_word = (WORD)param1 + data;

            if((Flags & FLAG_C) == FLAG_C){
                temp_word++;
            }

            Registers[REGISTER_A] = temp_word;

            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_v(param1, data, (BYTE)temp_word, 1);
            set_flag_n((BYTE)temp_word);
            set_flag_c(temp_word);

            break;

        case 0x93: /// <----- SBI - #
            param1 = Registers[REGISTER_A];
            param2 = fetch();
            temp_word = (WORD)param1 - param2;

            if((Flags & FLAG_C) == FLAG_C){
                temp_word--;
            }

            Registers[REGISTER_A] = (BYTE)temp_word;

            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_v(param1, param2, (BYTE)temp_word, 0);
            set_flag_n((BYTE)temp_word);
            set_flag_c(temp_word);

            break;

        case 0xA3: /// <----- CPI - #
            param1 = Registers[REGISTER_A];
            data = fetch();

            temp_word = (WORD)param1 - data;

            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_v(param1, data, (BYTE)temp_word, 0 );
            set_flag_n((BYTE)temp_word);
            set_flag_c(temp_word);
            break;

        case 0xB3: /// <----- ORI - #
            param1 = Registers[REGISTER_A];
            param2 = fetch();
            temp_word = param1 | param2;

            Registers[REGISTER_A] = (BYTE)temp_word;

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0xC3: /// <----- ANI - #
            param1 = Registers[REGISTER_A];
            param2 = fetch();
            temp_word = param1 & param2;

            Registers[REGISTER_A] = (BYTE)temp_word;

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0xD3: /// <----- XRI - #
            param1 = Registers[REGISTER_A];
            param2 = fetch();
            temp_word = param1 ^ param2;

            Registers[REGISTER_A] = (BYTE)temp_word;

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;

        case 0x26: /// <----- TST - abs
            address = abs_addressing();

            temp_word = Memory[address] - 0;

            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_n((BYTE)temp_word);
            break;

        case 0x27: // TST - abs,X
            address = abs_x_addressing();

            temp_word = Memory[address] - 0;

            //set flags
            set_flag_z((BYTE)temp_word);
            set_flag_n((BYTE)temp_word);
            break;

        case 0x28: /// <----- TSTA - A
            temp_word = Registers[REGISTER_A] - 0;

            Registers[REGISTER_A] = (BYTE)temp_word;

            //set flags
            set_flag_z(Registers[REGISTER_A]);
            set_flag_n(Registers[REGISTER_A]);
            break;



        case 0xEC: /// <----- SWI - impl
            psh_settings(Registers[REGISTER_A]);
            psh_settings(ProgramCounter);
            psh_settings(ProgramCounter >> 8);
            psh_settings(Flags);
            psh_settings(Registers[REGISTER_B]);
            psh_settings(Registers[REGISTER_C]);
            psh_settings(Registers[REGISTER_D]);
            psh_settings(Registers[REGISTER_E]);
            psh_settings(Registers[REGISTER_F]);
            //set interrupt flag
            Flags = Flags | FLAG_I;
            break;

        case 0xFC: /// <----- RTI - impl
            pop_settings(Registers[REGISTER_F]);
            pop_settings(Registers[REGISTER_E]);
            pop_settings(Registers[REGISTER_D]);
            pop_settings(Registers[REGISTER_C]);
            pop_settings(Registers[REGISTER_B]);
            pop_settings(Flags);
            pop_settings(ProgramCounter >> 8);
            pop_settings(ProgramCounter);
            pop_settings(Registers[REGISTER_A]);
            break;

        case 0xE2: /// <----- NOP
            break;

        case 0xF2: /// <----- HLT
            halt = true;
            break;
    }
}

void Group_2_Move(BYTE opcode){
    int destReg = 0;
    int sourceReg = 0;

    BYTE source = opcode & 0x0F;
    BYTE destination = (opcode >> 4);

    switch(source){
        case 0x09:
            sourceReg = REGISTER_A;
            break;

        case 0x0A:
            sourceReg = REGISTER_B;
            break;

        case 0x0B:
            sourceReg = REGISTER_C;
            break;

        case 0x0C:
            sourceReg = REGISTER_D;
            break;

        case 0x0D:
            sourceReg = REGISTER_E;
            break;

        case 0x0E:
            sourceReg = REGISTER_F;
            break;

        default:
            break;
    }

    switch(destination){
        case 0x05:
            destReg = REGISTER_A;
            break;

        case 0x06:
            destReg = REGISTER_B;
            break;

        case 0x07:
            destReg = REGISTER_C;
            break;

        case 0x08:
            destReg = REGISTER_D;
            break;

        case 0x09:
            destReg = REGISTER_E;
            break;

        case 0x0A:
            destReg = REGISTER_F;
            break;

        default:
            break;
    }
    //implementing Mv
    Registers[destReg] = Registers[sourceReg];

}
void execute(BYTE opcode)
{	
    if(opcode <= 0xFF){
        if ((opcode >= 0x59) && (opcode <= 0x5E) ||
            (opcode >= 0x69) && (opcode <= 0x6E) ||
            (opcode >= 0x79) && (opcode <= 0x7E) ||
            (opcode >= 0x89) && (opcode <= 0x8E) ||
            (opcode >= 0x99) && (opcode <= 0x9E) ||
            (opcode >= 0xA9) && (opcode <= 0xAE)){
            Group_2_Move(opcode);
        }
        else{
            Group_1(opcode);
        }
    }
}

void emulate()
{
	BYTE opcode;
    int runcount = 0;

	ProgramCounter = 0;
	halt = false;
	memory_in_range = true;

	printf("                    A  B  C  D  E  F  X  Z    SP\n");

	while ((!halt) && (memory_in_range) && (runcount != 213)) {
		printf("%04X ", ProgramCounter);           // Print current address
		opcode = fetch();
		execute(opcode);

		printf("%s  ", opcode_mneumonics[opcode]);  // Print current opcode

		printf("%02X ", Registers[REGISTER_A]);
		printf("%02X ", Registers[REGISTER_B]);
		printf("%02X ", Registers[REGISTER_C]);
		printf("%02X ", Registers[REGISTER_D]);
		printf("%02X ", Registers[REGISTER_E]);
		printf("%02X ", Registers[REGISTER_F]);
		printf("%02X ", IndexRegister);
		printf("%04X ", StackPointer);              // Print Stack Pointer

		if ((Flags & FLAG_Z) == FLAG_Z)	
		{
			printf("Z=1 ");
		}
		else
		{
			printf("Z=0 ");
		}
		if ((Flags & FLAG_V) == FLAG_V)	
		{
			printf("V=1 ");
		}
		else
		{
			printf("V=0 ");
		}
		if ((Flags & FLAG_I) == FLAG_I)	
		{
			printf("I=1 ");
		}
		else
		{
			printf("I=0 ");
		}
		if ((Flags & FLAG_N) == FLAG_N)	
		{
			printf("N=1 ");
		}
		else
		{
			printf("N=0 ");
		}
		if ((Flags & FLAG_C) == FLAG_C)	
		{
			printf("C=1 ");
		}
		else
		{
			printf("C=0 ");
		}
        runcount++;
		printf("\n");  // New line
	}

	printf("\n");  // New line
}


////////////////////////////////////////////////////////////////////////////////
//                            Simulator/Emulator (End)                        //
////////////////////////////////////////////////////////////////////////////////


void initialise_filenames() {
	int i;

	for (i=0; i<MAX_FILENAME_SIZE; i++) {
		hex_file [i] = '\0';
		trc_file [i] = '\0';
	}
}




int find_dot_position(char *filename) {
	int  dot_position;
	int  i;
	char chr;

	dot_position = 0;
	i = 0;
	chr = filename[i];

	while (chr != '\0') {
		if (chr == '.') {
			dot_position = i;
		}
		i++;
		chr = filename[i];
	}

	return (dot_position);
}


int find_end_position(char *filename) {
	int  end_position;
	int  i;
	char chr;

	end_position = 0;
	i = 0;
	chr = filename[i];

	while (chr != '\0') {
		end_position = i;
		i++;
		chr = filename[i];
	}

	return (end_position);
}


bool file_exists(char *filename) {
	bool exists;
	FILE *ifp;

	exists = false;

	if ( ( ifp = fopen( filename, "r" ) ) != NULL ) {
		exists = true;

		fclose(ifp);
	}

	return (exists);
}



void create_file(char *filename) {
	FILE *ofp;

	if ( ( ofp = fopen( filename, "w" ) ) != NULL ) {
		fclose(ofp);
	}
}



bool getline(FILE *fp, char *buffer) {
	bool rc;
	bool collect;
	char c;
	int  i;

	rc = false;
	collect = true;

	i = 0;
	while (collect) {
		c = getc(fp);

		switch (c) {
		case EOF:
			if (i > 0) {
				rc = true;
			}
			collect = false;
			break;

		case '\n':
			if (i > 0) {
				rc = true;
				collect = false;
				buffer[i] = '\0';
			}
			break;

		default:
			buffer[i] = c;
			i++;
			break;
		}
	}

	return (rc);
}






void load_and_run(int args,_TCHAR** argv) {
	char chr;
	int  ln;
	int  dot_position;
	int  end_position;
	long i;
	FILE *ifp;
	long address;
	long load_at;
	int  code;

	// Prompt for the .hex file

	printf("\n");
	printf("Enter the hex filename (.hex): ");

	if(args == 2){
		ln = 0;
		chr = argv[1][ln];
		while (chr != '\0')
		{
			if (ln < MAX_FILENAME_SIZE)
			{
				hex_file [ln] = chr;
				trc_file [ln] = chr;
				ln++;
			}
			chr = argv[1][ln];
		}
	} else {
		ln = 0;
		chr = '\0';
		while (chr != '\n') {
			chr = getchar();

			switch(chr) {
			case '\n':
				break;
			default:
				if (ln < MAX_FILENAME_SIZE)	{
					hex_file [ln] = chr;
					trc_file [ln] = chr;
					ln++;
				}
				break;
			}
		}

	}
	// Tidy up the file names

	dot_position = find_dot_position(hex_file);
	if (dot_position == 0) {
		end_position = find_end_position(hex_file);

		hex_file[end_position + 1] = '.';
		hex_file[end_position + 2] = 'h';
		hex_file[end_position + 3] = 'e';
		hex_file[end_position + 4] = 'x';
		hex_file[end_position + 5] = '\0';
	} else {
		hex_file[dot_position + 0] = '.';
		hex_file[dot_position + 1] = 'h';
		hex_file[dot_position + 2] = 'e';
		hex_file[dot_position + 3] = 'x';
		hex_file[dot_position + 4] = '\0';
	}

	dot_position = find_dot_position(trc_file);
	if (dot_position == 0) {
		end_position = find_end_position(trc_file);

		trc_file[end_position + 1] = '.';
		trc_file[end_position + 2] = 't';
		trc_file[end_position + 3] = 'r';
		trc_file[end_position + 4] = 'c';
		trc_file[end_position + 5] = '\0';
	} else {
		trc_file[dot_position + 0] = '.';
		trc_file[dot_position + 1] = 't';
		trc_file[dot_position + 2] = 'r';
		trc_file[dot_position + 3] = 'c';
		trc_file[dot_position + 4] = '\0';
	}

	if (file_exists(hex_file)) {
		// Clear Registers and Memory

		Registers[REGISTER_A] = 0;
		Registers[REGISTER_B] = 0;
		Registers[REGISTER_C] = 0;
		Registers[REGISTER_D] = 0;
		Registers[REGISTER_E] = 0;
		Registers[REGISTER_F] = 0;
		IndexRegister = 0;
		Flags = 0;
		ProgramCounter = 0;
		StackPointer = 0;

		for (i=0; i<MEMORY_SIZE; i++) {
			Memory[i] = 0x00;
		}

		// Load hex file

		if ( ( ifp = fopen( hex_file, "r" ) ) != NULL ) {
			printf("Loading file...\n\n");

			load_at = 0;

			while (getline(ifp, InputBuffer)) {
				if (sscanf(InputBuffer, "L=%x", &address) == 1) {
					load_at = address;
				} else if (sscanf(InputBuffer, "%x", &code) == 1) {
					if ((load_at >= 0) && (load_at <= MEMORY_SIZE)) {
						Memory[load_at] = (BYTE)code;
					}
					load_at++;
				} else {
					printf("ERROR> Failed to load instruction: %s \n", InputBuffer);
				}
			}

			fclose(ifp);
		}

		// Emulate

		emulate();
	} else {
		printf("\n");
		printf("ERROR> Input file %s does not exist!\n", hex_file);
		printf("\n");
	}
}

void building(int args,_TCHAR** argv){
	char buffer[1024];
	load_and_run(args,argv);
	sprintf(buffer, "0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X", 
		Memory[TEST_ADDRESS_1],
		Memory[TEST_ADDRESS_2],
		Memory[TEST_ADDRESS_3],
		Memory[TEST_ADDRESS_4], 
		Memory[TEST_ADDRESS_5],
		Memory[TEST_ADDRESS_6], 
		Memory[TEST_ADDRESS_7],
		Memory[TEST_ADDRESS_8], 
		Memory[TEST_ADDRESS_9], 
		Memory[TEST_ADDRESS_10],
		Memory[TEST_ADDRESS_11],
		Memory[TEST_ADDRESS_12]
		);
	sendto(sock, buffer, strlen(buffer), 0, (SOCKADDR *)&server_addr, sizeof(SOCKADDR));
}



void test_and_mark() {
	char buffer[1024];
	bool testing_complete;
	unsigned int len = sizeof(SOCKADDR);
	char chr;
	int  i;
	int  j;
	bool end_of_program;
	long address;
	long load_at;
	int  code;
	int  mark;
	int  passed;

	printf("\n");
	printf("Automatic Testing and Marking\n");
	printf("\n");

	testing_complete = false;

	sprintf(buffer, "Test Student %s", STUDENT_NUMBER);
	sendto(sock, buffer, strlen(buffer), 0, (SOCKADDR *)&server_addr, sizeof(SOCKADDR));

	while (!testing_complete) {
		memset(buffer, '\0', sizeof(buffer));

		if (recvfrom(sock, buffer, sizeof(buffer)-1, 0, (SOCKADDR *)&client_addr, &len) != SOCKET_ERROR) {
			printf("Incoming Data: %s \n", buffer);

			//if (strcmp(buffer, "Testing complete") == 1)
			if (sscanf(buffer, "Testing complete %d", &mark) == 1) {
				testing_complete = true;
				printf("Current mark = %d\n", mark);

			}else if (sscanf(buffer, "Tests passed %d", &passed) == 1) {
				//testing_complete = true;
				printf("Passed = %d\n", passed);

			} else if (strcmp(buffer, "Error") == 0) {
				printf("ERROR> Testing abnormally terminated\n");
				testing_complete = true;
			} else {
				// Clear Registers and Memory

		Registers[REGISTER_A] = 0;
		Registers[REGISTER_B] = 0;
		Registers[REGISTER_C] = 0;
		Registers[REGISTER_D] = 0;
		Registers[REGISTER_E] = 0;
		Registers[REGISTER_F] = 0;
		IndexRegister = 0;
				Flags = 0;
				ProgramCounter = 0;
				StackPointer = 0;
				for (i=0; i<MEMORY_SIZE; i++) {
					Memory[i] = 0;
				}

				// Load hex file

				i = 0;
				j = 0;
				load_at = 0;
				end_of_program = false;
				FILE *ofp;
				fopen_s(&ofp ,"branch.txt", "a");

				while (!end_of_program) {
					chr = buffer[i];
					switch (chr) {
					case '\0':
						end_of_program = true;

					case ',':
						if (sscanf(InputBuffer, "L=%x", &address) == 1) {
							load_at = address;
						} else if (sscanf(InputBuffer, "%x", &code) == 1) {
							if ((load_at >= 0) && (load_at <= MEMORY_SIZE)) {
								Memory[load_at] = (BYTE)code;
								fprintf(ofp, "%02X\n", (BYTE)code);
							}
							load_at++;
						} else {
							printf("ERROR> Failed to load instruction: %s \n", InputBuffer);
						}
						j = 0;
						break;

					default:
						InputBuffer[j] = chr;
						j++;
						break;
					}
					i++;
				}
				fclose(ofp);
				// Emulate

				if (load_at > 1) {
					emulate();
					// Send and store results
					sprintf(buffer, "%02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X", 
						Memory[TEST_ADDRESS_1],
						Memory[TEST_ADDRESS_2],
						Memory[TEST_ADDRESS_3],
						Memory[TEST_ADDRESS_4], 
						Memory[TEST_ADDRESS_5],
						Memory[TEST_ADDRESS_6], 
						Memory[TEST_ADDRESS_7],
						Memory[TEST_ADDRESS_8], 
						Memory[TEST_ADDRESS_9], 
						Memory[TEST_ADDRESS_10],
						Memory[TEST_ADDRESS_11],
						Memory[TEST_ADDRESS_12]
						);
					sendto(sock, buffer, strlen(buffer), 0, (SOCKADDR *)&server_addr, sizeof(SOCKADDR));
				}
			}
		}
	}
}


#if defined(WIN32)
int _tmain(int argc, _TCHAR* argv[])
#else
int main(int argc, _TCHAR* argv[])
#endif
{
	char chr;
	char dummy;

	printf("\n");
	printf("Microprocessor Emulator\n");
	printf("UWE Computer and Network Systems Assignment 1\n");
	printf("\n");

	initialise_filenames();

    #if defined(WIN32)
	if (WSAStartup(MAKEWORD(2, 2), &data) != 0) return(0);
    #endif

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);  // Here we create our socket, which will be a UDP socket (SOCK_DGRAM).
	if (!sock) {	
		// Creation failed! 
	}

	memset(&server_addr, 0, sizeof(SOCKADDR_IN));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(IP_ADDRESS_SERVER);
	server_addr.sin_port = htons(PORT_SERVER);

	memset(&client_addr, 0, sizeof(SOCKADDR_IN));
	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	client_addr.sin_port = htons(PORT_CLIENT);

	chr = '\0';
	while ((chr != 'e') && (chr != 'E'))
	{
		printf("\n");
		printf("Please select option\n");
		printf("L - Load and run a hex file\n");
		printf("T - Have the server test and mark your emulator\n");
		printf("E - Exit\n");
		if(argc == 2){ building(argc,argv); exit(0);}
		printf("Enter option: ");
		chr = getchar();
		if (chr != 0x0A)
		{
			dummy = getchar();  // read in the <CR>
		}
		printf("\n");

		switch (chr)
		{
		case 'L':
		case 'l':
			load_and_run(argc,argv);
			break;

		case 'T':
		case 't':
			test_and_mark();
			break;

		default:
			break;
		}
	}

	closesocket(sock);
    #if defined(WIN32)
	WSACleanup();
    #endif


	return 0;
}
